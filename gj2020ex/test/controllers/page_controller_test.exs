defmodule Gj2020ex.PageControllerTest do
  use Gj2020ex.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end
end
