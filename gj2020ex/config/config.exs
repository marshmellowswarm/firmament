# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :gj2020ex, Gj2020ex.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0eAE7vs6udKfzmllNXQ4eoivi9+LeoPIaMLaYncCQKFCLsOClCLQi7f3umzy4EuC",
  render_errors: [view: Gj2020ex.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Gj2020ex.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
