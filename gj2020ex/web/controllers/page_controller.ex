defmodule Gj2020ex.PageController do
  use Gj2020ex.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def screen(conn, _params) do
    render conn, "screen.html"
  end
end
