defmodule Gj2020ex.Router do
  use Gj2020ex.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Gj2020ex do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/screen", PageController, :screen
  end

  # Other scopes may use custom stacks.
  # scope "/api", Gj2020ex do
  #   pipe_through :api
  # end
end
