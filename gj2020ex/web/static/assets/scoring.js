let factoryNumber;
let score = 0;
let highScore = 0;
let totalThrough = 0;
let fixedThrough = 0;
let totalThroughs = [0.0001, 0.0001, 0.0001];
let fixedThroughs = [0.000, 0.000, 0.000];
let strings = [" boxes ", " phones ", " bikes "];


let scoreText;
let highScoreText;
const defaultScorePrefix = "   <==   The Factory #";
const defaultScoreMid = " has quality otput of ";
const defaultScoreMid2 = " average of ";
const defaultScoreSuffix = "%   ==>   ";
const defaultHighPrefix = "   !!   Our previos average record was ";
const defaultHighSuffix = "%   !!   DO YOUR PART TO HELP MAKE NEW RECORD  !!  ";

function loadScoring(game) {

}


function createScoring(scene) {
    var style = { font: 'bold 40px "Roboto Condensed"', fill: "#000000",
        boundsAlignH: "center", boundsAlignV: "middle", backgroundColor: "#ffffff"};
    scoreText = scene.add.text(50, 10,
        defaultScorePrefix  +factoryNumber + defaultScoreMid+ score + defaultScoreSuffix,
        style);
    scoreText.setDepth(2);
    highScoreText = scene.add.text(50, 90,
        defaultHighPrefix + highScore + defaultHighSuffix,
            style);

    //{ fontFamily: '"Roboto Condensed"', fontSize: 50}
}

function updateScoring(itemType, points) {
    score += 1;
    scoreText.text = defaultScorePrefix  +factoryNumber + defaultScoreMid+ score + defaultScoreSuffix;
}

function updateScoringOnItemOut(item) {
    if (item.repaired){
        fixedThrough++;
    }
    totalThrough++;

    let itemType = item.itemType;
    if (item.repaired){
        fixedThroughs[itemType]++;
    }
    totalThroughs[itemType]++;

    score = Math.round(fixedThrough*100/totalThrough);
    if (score > highScore){
        highScore = score;
    }
    //score += 1;

    let mytext = defaultScorePrefix  +factoryNumber + defaultScoreMid;
    for (var i=0; i<3; ++i){
        mytext += strings[i] +Math.round(fixedThroughs[i] * 100 / totalThroughs[i]) + "%, ";
    }
    mytext += defaultScoreMid2 + score + defaultScoreSuffix;

    scoreText.text = mytext;
    //defaultScorePrefix  +factoryNumber + defaultScoreMid+ score + defaultScoreSuffix;
    highScoreText.text = defaultHighPrefix + highScore + defaultHighSuffix;

}
