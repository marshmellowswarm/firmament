var iosHere = 0;
var mySound;
var initButton;
var bumpButton;
var inputGame;
var inputName;
var joinButton;
var playerColor = 'gray';
var channel = undefined;
var sounds = ['assets/beats/saw-1.mp3','assets/beats/hammer-1.mp3',
  'assets/beats/wrench-1.mp3'];
var colors = ['#FCDD35', '#3FA9F5', '#3FE800'];
var aims = ['Unsaw a box', 'Hit a phone', 'Wrench a bike'];

function setup() {

  
  createCanvas(0, 0);
  //stop();

  // initButton = createButton('click me');
  // initButton.position(19, 19);
  // initButton.mousePressed(initApp);

  // inputGame = createInput();
  // inputGame.position(10, 10);
  // inputName = createInput();
  // inputName.position(150, 10);
  // joinButton = createButton("Join game");
  // joinButton.position(300, 10);





  bumpButton = createButton('Join');
  bumpButton.position(windowWidth*0.1, windowHeight*0.3);
  bumpButton.mousePressed(joinGame);
  bumpButton.style('background-color', playerColor);

  bumpButton.style('margin', 'auto');
  bumpButton.style('width', '80%');
  bumpButton.style('height', '50%');
  bumpButton.style('font-size', '50px');
  bumpButton.style('border-radius', '50px');


  mySound = loadSound('sound.mp3');
}

function setNewSound(soundType){
  mySound = loadSound(sounds[soundType]);
  bumpButton.style('background-color', colors[soundType]);
  bumpButton.elt.textContent = aims[soundType];
}

function draw() {
  //var x = accelerationX;
  //iosHere = x*20;
  //iosHere = iosHere*0.99 + x*10*0.01;
  noLoop();
  background(iosHere);
}

function joinGame(){
  runAtStart();
  //bumpButton.mousePressed(playSoundAndSendMessage);
}

function initApp() {
  if (typeof DeviceMotionEvent.requestPermission === 'function') {
  // iOS 13+
    iosHere = 100;
    DeviceMotionEvent.requestPermission().then(response => {
  if (response == 'granted') {
      window.addEventListener('devicemotion', handleMotionEvent);
  }
      else{
        iosHere = 250;

      }
  }).catch(console.error)
  } else {
  // non iOS 13+
    window.addEventListener('devicemotion', handleMotionEvent);
  }
}

function playSoundAndSendMessage(){
  if (mySound.isPlaying()) {
    // .isPlaying() returns a boolean
    mySound.stop();
    //background(255, 0, 0);
  }
  {
    mySound.play();
    //background(0);
  }
  if (channel != undefined){
    channel.push("msg", "bump");
  }else{

    console.debug("something wrong");
  }
}

// function handleMotionEvent(event){
//   //var x = event.accelerationIncludingGravity.x;
//   var x = accelerationX;
//   iosHere = iosHere*0.9 + x*10*0.1;
//   bumpButton.elt.textContent = "g" + x;
// }

// function makeNewName(msg){
//   bumpButton.elt.textContent = msg;
// }
