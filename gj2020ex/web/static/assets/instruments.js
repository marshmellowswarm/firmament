


function loadInstruments(game) {
    // load sound files..
    // load items??
    game.load.audio('inst1', [
        'assets/beats/saw-1.mp3'        
    ]);
    game.load.audio('inst2', [
        'assets/beats/hammer-1.mp3'        
    ]);
    game.load.audio('inst3', [
        'assets/beats/wrench-1.mp3'        
    ]);
}

function createInstruments(scene) {
    // hardcode to 3 instruments
    scene.instruments = {
        time1: 0,
        time2: 0,
        time3: 0,
        timeOffset1: 0, // might be some magic number here??
        timeOffset2: 0,
        timeOffset3: 0,
        sounds: []
    };

    scene.instruments.sounds.push(scene.sound.add('inst1'))
    scene.instruments.sounds.push(scene.sound.add('inst2'))
    scene.instruments.sounds.push(scene.sound.add('inst3'))
}


function updateInstruments(scene, time, delta) {

    scene.instruments.time1 += delta;
    scene.instruments.time2 += delta;
    scene.instruments.time3 += delta;
    if (scene.instruments.time1 > scene.bpsInMillis() + scene.bpsInMillis() * scene.instruments.timeOffset1) {
        let it = scene.beats.instruments[0].shift();
        if (it != 0) {
            fallingItems.add(0, it);
        }
        scene.instruments.time1 = 0
        //scene.instruments.sounds[0].play()
    }

    if (scene.instruments.time2 > scene.bpsInMillis() + scene.bpsInMillis() * scene.instruments.timeOffset2) {
        let it = scene.beats.instruments[1].shift();
        if (it != 0) {
            fallingItems.add(1, it);
        }
        scene.instruments.time2 = 0
        //scene.instruments.sounds[1].play()
    }

    if (scene.instruments.time3 > scene.bpsInMillis() + scene.bpsInMillis() * scene.instruments.timeOffset3) {
        let it = scene.beats.instruments[2].shift();
        if (it != 0) {
            fallingItems.add(2, it);
        }
        scene.instruments.time3 = 0
        //scene.instruments.sounds[2].play()
    }
}