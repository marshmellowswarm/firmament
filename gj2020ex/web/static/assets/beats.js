


function loadBeats(game) {
    game.load.text('beats', 'assets/beats/beats.csv');
}

function createBeats(scene) {
    let cache = scene.cache.text;
    let csvString = cache.get('beats');

    let beats = Papa.parse(csvString);
    console.log("Parsed CSV file");
    console.log(beats);

    scene.bps = 8;
    scene.bpsInMillis = function() {
        return (1 / scene.bps * 1000);
    }
    scene.beats = beats;
    scene.beatTime = 0.0;
    // first line is header in file
    scene.beatNumber = 1;
    scene.beats.instruments = [];
    for (let i = 0; i < scene.beats.data[0].length; i++) {
        let instrumentBeats = [];
        scene.beats.instruments.push(instrumentBeats);
    }

    scene.interval = 0

}

function updateBeats(scene, time, delta) {

    scene.beatTime += delta;
    if (scene.beatTime > scene.bpsInMillis()) {
        scene.beatTime = 0.0;

        let beatLine = scene.beats.data[scene.beatNumber];
        //console.log("beatLine[" + scene.beatNumber + "]", beatLine);
        for (let i = 0; i < beatLine.length; i++) {
            scene.beats.instruments[i].push(beatLine[i]);
        }

        scene.beatNumber++;
        if (scene.beatNumber >= scene.beats.data.length) {
            scene.beatNumber = 1;
        }
    }

    if (scene.interval == 0) {
        scene.interval = time
    } else {
        if (scene.interval - time > 5000) {
            incrementBeats(scene)
            scene.interval = time
        } 
    }
}



function incrementBeats(scene) {
    scene.bps++;
}

function decrementBeats(scene) {
    if (scene.bps != 6) {
        scene.bps--;
    }    
}