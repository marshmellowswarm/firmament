const BUTTONS = [
    Phaser.Input.Keyboard.KeyCodes.ONE,
    Phaser.Input.Keyboard.KeyCodes.TWO,
    Phaser.Input.Keyboard.KeyCodes.THREE,
    Phaser.Input.Keyboard.KeyCodes.FOUR,
    Phaser.Input.Keyboard.KeyCodes.FIVE,
    Phaser.Input.Keyboard.KeyCodes.SIX,
    Phaser.Input.Keyboard.KeyCodes.SEVEN,
    Phaser.Input.Keyboard.KeyCodes.EIGHT,
    Phaser.Input.Keyboard.KeyCodes.NINE,
    Phaser.Input.Keyboard.KeyCodes.Q,
    Phaser.Input.Keyboard.KeyCodes.W,
    Phaser.Input.Keyboard.KeyCodes.E,
    Phaser.Input.Keyboard.KeyCodes.R,
    Phaser.Input.Keyboard.KeyCodes.T,
    Phaser.Input.Keyboard.KeyCodes.Y
];



class PlayerAnimation {
    constructor(animationKey, imageKey, spritePath, frameInfo){
        this.animationKey = animationKey;
        this.imageKey = imageKey;
        this.spritePath = spritePath;
        this.frameInfo = frameInfo;
    }
}

const PLAYERPOSMATRIX = [
    {x:180, y:430},{x:530, y:630},{x:900, y:830},{x:1250, y:430},{x:1600, y:630},
    {x:180, y:830},{x:530, y:430},{x:900, y:630},{x:1250, y:830},{x:1600, y:430},
    {x:180, y:630},{x:530, y:830},{x:900, y:430},{x:1250, y:630},{x:1600, y:830}
]

const PLAYERANIMATIONS = [
    new PlayerAnimation("work1", "guy1", "assets/man/animationGuy1.png",  { frameWidth: 225, frameHeight: 153 }),
    new PlayerAnimation("work2", "guy2", "assets/man/animationGuy2.png",  { frameWidth: 167, frameHeight: 217 }),
    new PlayerAnimation("work3", "guy3", "assets/man/animationGuy3.png",  { frameWidth: 153, frameHeight: 153 }),
];

const loadPlayers = (phaser) => {
    for(let i in PLAYERANIMATIONS) {
        const p = PLAYERANIMATIONS[i];
        console.log(`${p.imageKey}: ${p.spritePath}`)
        phaser.load.spritesheet(p.imageKey, p.spritePath, p.frameInfo);
    }
};

 class Players {
    constructor(phaser, fallingItemGroup) {
        this.playerList = [];
        this.phaser = phaser;
        this.fallingItemGroup = fallingItemGroup;
    }

    fire(playerName) {
        console.log(`bang ${playerName}`);
        let player = undefined;
        let playerNum = -1;
        for(let i in this.playerList) {
            if(this.playerList[i].name === playerName) {
                player = this.playerList[i];
                playerNum = i;
                break;
            }
        }

        if(!player) {
            console.warn(`Player with name ${playerName} is not registered with game`);
            return;
        }

        if(player.inFire) {
            console.log(`player ${playerName} is already firing, ignoring this fire command`);
            return
        } else {
            player.inFire = true;
        }

        this.fallingItemGroup.children.each((child) => {
            const distToChild = player.sprite.body.center.distance(child.body.center);
            const distDiffTop =  -player.sprite.body.top + child.body.top;
            const distDiffBottom = player.sprite.body.bottom - child.body.bottom;
            const playerMiddle = player.sprite.body.bottom - player.sprite.body.halfHeight;
            const itemMiddle = child.body.bottom - child.body.halfHeight;
            const distMiddle = playerMiddle - itemMiddle;
            const heightDiff = (player.sprite.body.height - child.body.height)/4;
            const playerHeight = player.sprite.body.height;
            //TODO work on magic constants
            //if(distToChild < 110) {
            //if (distDiffTop > 30 && distDiffBottom > 60){
            //if (distDiffTop > heightDiff 
            //    && distDiffBottom > heightDiff*2 
            if (distMiddle > -40 && distMiddle < 40
                && distToChild < 180)
                {

                console.log("player.playerType " + player.playerType);
                console.log("child.itemType " + child.itemType);
                if (player.playerType == child.itemType) {
                    console.log(`player ${player.name} hit(distance: ${distToChild}!`);
                    console.log(child)
                    player.instrument.play();
                    child.anims.play(child.itype.animationKey + "Fixed", true);
                    child.repaired = true;
                    // move remove to conveyor end
                    //this.fallingItemGroup.remove(child, true, true);
                    //updateScoring(child, 1);
                }
            }
        });

        console.log("firing: " + playerNum);
        player.sprite.anims.play(PLAYERANIMATIONS[this.getPlayerAnimationIndex(playerNum)].animationKey, true);
        setTimeout(() => {
                console.log("done firing: " + playerNum);
                player.sprite.anims.stop();
                player.inFire = false;
            }, 300);

    }

    getPlayerAnimationIndex(playerNum) {
        return playerNum % PLAYERANIMATIONS.length;
    }

    add(name) {
        for (var i = 0; i<this.playerList.length; ++i){
            if (this.playerList[i].name == name){
                return;
            }
        }

        game.title.destroy();
        console.log(`adding player ${name}`);
        const playerNum = this.playerList.length;

        if(playerNum > BUTTONS.length) {
            throw `Not enough buttons to be able to add player ${name}`;
            return;
        }

        let playerType = this.getPlayerAnimationIndex(playerNum);
        console.log("playerType: " + playerType)

        let pos = PLAYERPOSMATRIX[playerNum]
        const xPos = pos.x
        const yPos = pos.y
        const sprite = this.phaser.physics.add.staticSprite(xPos, yPos, PLAYERANIMATIONS[playerType].imageKey).setScale(0.5, 0.5);
        const text = this.phaser.add.text(xPos - 70, yPos + 55, name, { fontFamily: '"Roboto Condensed"' });


        let button = this.phaser.input.keyboard.addKey(BUTTONS[playerNum]);
        button.on("down", () => {
            this.fire(name);
        });

        this.phaser.anims.create({
            key: PLAYERANIMATIONS[playerType].animationKey,
            frames: this.phaser.anims.generateFrameNumbers(PLAYERANIMATIONS[playerType].imageKey, { start: 0, end: 3 }),
            frameRate: 10,
            repeat: 0
        });

        this.playerList.push(
            {name: name, sprite: sprite, button: button, inFire: false, playerType: playerType,
             instrument: this.phaser.instruments.sounds[playerType]}
        );
        console.log(`Done making player ${name}`);
        return playerType;
    }
}
