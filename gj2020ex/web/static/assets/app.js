var config = {
    type: Phaser.AUTO,
    width: 1920,
    height: 1080,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

const totalSounds = 2;
let players = undefined;
let fallingItems = undefined;
let conveyorbelts = undefined;

function preload() {

    this.load.image('title', 'assets/title.png');
    this.load.image('background', 'assets/background.png');
    this.load.spritesheet('note', 'assets/note.png', { frameWidth: 80, frameHeight: 20 });
    this.load.audio('theme', [
        'assets/NK008-CBA.mp3'
    ]);
    this.load.audio('beep', [
        //'assets/sound.mp3'
        'assets/sample1.m4a'
    ]);

    loadPlayers(this);
    loadBeats(this)
    loadInstruments(this)
    loadFallingItems(this);
    loadScoring(this);
    Conveyorbelts.preload(this);
}

function create() {

    createBeats(this);
    createInstruments(this);
    createScoring(this);

    this.add.image(0, 0, 'background').setOrigin(0, 0);

    game.title = this.add.image(0, 0, 'title').setOrigin(0, 0);
    game.title.setDisplaySize (1920, 1080);
    game.title.setDepth(1);

    fallingItems = new FallingItems(this);

    players = new Players(this, fallingItems.itemGroup);
    if(createFakePlayers) {
        
        players.add("Player1");
       
        players.add("Player2");
        players.add("Player3");
         /*
        players.add("Player4");
          
        players.add("Player5");
        players.add("Player6");
        players.add("Player7");
        players.add("Player8");
        players.add("Player9");
        */
        /*
        players.add("Player10");
        players.add("Player11");
        players.add("Player12");
        players.add("Player13");
        players.add("Player14");
        players.add("Player15");
        */
    }
    this.players = players

    conveyorbelts = new Conveyorbelts(this, 30, 30, 130, 970);

    var music = this.sound.add('theme');
    //music.play();
}

function update(time, delta) {

    if (this.players.playerList.length < 1) {
        console.log("Waiting for players...")
        return
    }
    updateBeats(this, time, delta)
    updateInstruments(this, time, delta)
    conveyorbelts.update(this, fallingItems.itemGroup);
}
