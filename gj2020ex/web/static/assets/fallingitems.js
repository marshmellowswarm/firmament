
class ItemType {
    constructor(animationKey, imageKey, animation, scaleX, scaleY, offset, frameInfo) {
        this.animationKey = animationKey;
        this.imageKey = imageKey;
        this.animation = animation;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.offset = offset;
        this.frameInfo = frameInfo;
    }
}
const ITEMTYPES = [
    new ItemType("boxAnim", "boxImg", "./assets/items/boxAnim.png", 0.35, 0.35, 30, { frameWidth: 145, frameHeight: 72 }),
    new ItemType("phoneAnim", "phoneImg", "./assets/items/phoneAnim.png", 0.35, 0.35, 10, { frameWidth: 76, frameHeight: 122 }),
    new ItemType("bikeAnim", "bikeImg", "./assets/items/bikeAnim.png", 0.4, 0.4, -10, { frameWidth: 97, frameHeight: 156 }),

];

function loadFallingItems(phaser) {
    for (let i in ITEMTYPES) {
        phaser.load.spritesheet(ITEMTYPES[i].imageKey, ITEMTYPES[i].animation, ITEMTYPES[i].frameInfo);
    }
}

class FallingItems {
    constructor(phaser) {
        this.phaser = phaser;
        this.itemGroup = phaser.physics.add.group();
    }

    // number from csv file that identifies item. Generate on all active conveyor belts
    // both arguments are numbers
    // TODO: implement use of pitch
    add(item, pitch) {


        let belts = getBeltsToSpawnOn(item, this.phaser)
        console.log("Item type " + item)
        console.log(belts)
        for (let i = 0; i < belts.length; i++) {
            this.spawnItem(item, belts[i], pitch)
        }
    }

/*

Item type 1 fallingitems.js:39:17
Array [ 1 ]
fallingitems.js:40:17
Item type 0 fallingitems.js:39:17
Array [ 0, 3 ]
fallingitems.js:40:17
Item type 2 fallingitems.js:39:17
Array [ 2 ]

*/

    spawnItem(item, beltNumber, pitch) {
        //console.log(`adding item ${item}, ${pitch}`);
        const yPos = 230;
        const xPos = 200 + (370 * beltNumber) + ITEMTYPES[item].offset;
        const itype = ITEMTYPES[item];
        const sprite = this.itemGroup.create(xPos, yPos, itype.imageKey).setScale(ITEMTYPES[item].scaleX, ITEMTYPES[item].scaleY);

        //console.log("bpsinmillis: " + this.phaser.bpsInMillis())
        sprite.setVelocity(0, 200);
        sprite.itype = itype;
        sprite.itemType = item;
        sprite.repaired = false;
        sprite.pitch = pitch;
        sprite.broken = this.phaser.anims.create({
            key: itype.animationKey,
            frames: this.phaser.anims.generateFrameNumbers(itype.imageKey, { start: 0, end: 0 }),
            frameRate: 10,
            repeat: 0
        });

        sprite.fixed = this.phaser.anims.create({
            key: itype.animationKey + "Fixed",
            frames: this.phaser.anims.generateFrameNumbers(itype.imageKey, { start: 1, end: 1 }),
            frameRate: 10,
            repeat: 0
        });

        sprite.anims.play(itype.animationKey, true);
    }
}


const BOXES_SPAWN = [
    [0], [0], [0],
    [0, 3], [0, 3], [0, 3],
    [0, 1, 3], [0, 1, 3], [0, 1, 3],
    [0, 1, 3, 4], [0, 1, 3, 4], [0, 1, 3, 4],
    [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4],
]

const PHONES_SPAWN = [
    [], [1], [1],
    [1], [1, 4], [1, 4],
    [1, 4], [1, 2, 4], [1, 2, 4],
    [1, 2, 4], [0, 1, 2, 4], [0, 1, 2, 4],
    [0, 1, 2, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4],
]

const BIKES_SPAWN = [
    [], [], [2],
    [2], [2], [0, 2],
    [0, 2], [0, 2], [0, 2, 3],
    [0, 2, 3], [0, 2, 3], [0, 1, 2, 3],
    [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3, 4],
]

function getBeltsToSpawnOn(itemType, phaser) {

    let numberOfPlayers = phaser.players.playerList.length
    if (numberOfPlayers < 1) {
        let belts = []
        return belts
    }
    if (itemType == 0) {
        return BOXES_SPAWN[numberOfPlayers - 1]
    }
    if (itemType == 1) {
        return PHONES_SPAWN[numberOfPlayers - 1]
    }
    if (itemType == 2) {
        return BIKES_SPAWN[numberOfPlayers - 1]
    }
}
