

class Conveyorbelts {

static preload(scene) {
    // intentionally left blank for now
}

constructor(scene, upperLeftX, upperLeftY, lowerRightX, lowerRigthY) {
    this.bottom = lowerRigthY;
}

// to check if items have fallen off.
update(scene, fallingItemGroup) {
    // TODO: To improve performance, consider replacing this logic with with
    // adding a line that collisions-interacts with falling items, and on
    // detecteded collission does what the bottom-dection does now
    fallingItemGroup.children.each((child) => {
        const hasCenterPassedBeltBottom = child.body.center.y > this.bottom;
        if(hasCenterPassedBeltBottom) {
            //console.log(`child.body.center.y ${child.body.center.y}`);
            //console.log("child.itemType passed conveyor belt " + child.itemType);
            updateScoringOnItemOut(child);
            fallingItemGroup.remove(child, true, true);
            decrementBeats(scene);

        }
    });
}
}