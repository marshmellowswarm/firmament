// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "web/static/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/my_app/endpoint.ex":
import {Socket} from "phoenix"

let me
let players = {}
let messagesContainer = $("#messages")
let canvas = $("#canvas")[0]
let ctx = canvas.getContext("2d")
let w = $("#canvas").width()
let h = $("#canvas").height()
let playerWidth = 20
let playerHeight = 20

// draw board
function drawBoard() {
  ctx.fillStyle = "white"
  ctx.fillRect(0, 0, w, h)
  for (let id in players) {
    drawPlayer(players[id])
  }
}

// draw a player
function drawPlayer(player) {
  let x = player.x * playerWidth
  let y = player.y * playerHeight
  ctx.fillStyle = "blue"
  ctx.fillRect(x, y, playerWidth, playerHeight)
  ctx.strokeStyle = "white"
  ctx.strokeRect(x, y, playerWidth, playerHeight)
}

function setupChannelMessageHandlers(channel) {
  // New player joined the game
  channel.on("player:joined", ({player: player}) => {
    messagesContainer.append(`<br/>${player.id} joined`)
    messagesContainer.scrollTop( messagesContainer.prop("scrollHeight"))
    players[player.id] = player
    drawBoard()
  })  // Player changed position in board
  channel.on("player:position", ({player: player}) => {
    players[player.id] = player
    drawBoard()
  })
}

// Maps the arrow keys to a direction
function bindArrowKeys(channel, document) {
  $(document).keydown(function(e) {
    let key = e.which, d
    if(key == "37") {
      d = "left"
    } else if(key == "38") {
      d = "up"
    } else if(key == "39") {
      d = "right"
    } else if(key == "40") {
      d = "down"
    }    if (d) {
      // notifies everyone our move
      channel.push("player:move", {direction: d})
    }
  });
}

// Start the connection to the socket and joins the channel
// Does initialization and key binding
function connectToSocket(user_id, document) {
  // connects to the socket endpoint
  let socket = new Socket("/socket", {params: {user_id: user_id}})
  socket.connect()
  let channel = socket.channel("players:lobby", {})
  me = user_id

  // joins the channel
  channel.join()
    .receive("ok", initialPlayers => { // on joining channel, we receive the current players list
      console.log('Joined to channel');
      setupChannelMessageHandlers(channel)
      bindArrowKeys(channel, document)
      players = initialPlayers.players
      drawBoard()
    })
}

export {connectToSocket}
