// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

import {Socket} from "phoenix"

export var App = {
  setupScreen: function(onconnect){
    console.log("starting")
    let socket = new Socket("/screensocket")

    socket.connect()
    let channel = socket.channel("screen:lobby", {})
    // joins the channel
    channel.join()
      .receive("ok", ({game_id: game_id}) => {

        console.log('Started with id ', game_id);

        let screenChan = socket.channel("screen:" + game_id)
        screenChan.join()
          .receive("ok", msg => {
              onconnect(screenChan, game_id)
          })

      })
      .receive("error", error => { console.log("error", error) })
  },
  setupPlayer: function(name, game_id) {
      console.log("starting")
      let socket = new Socket("/playersocket", {params: {user_id: name}})
      socket.connect()
      let channel = socket.channel("players:" + game_id)
      return channel
  }
}
