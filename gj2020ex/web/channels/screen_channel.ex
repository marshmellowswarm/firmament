defmodule Game.ScreenChannel do
  use Phoenix.Channel

  alias Game.GameState

  def join("screen:lobby", message, socket) do
    game_id = socket.assigns.game_id
    #GameState.put_game(game_id, socket)
    #send(self(), {:after_join, message})
    {:ok, %{game_id: socket.assigns.game_id}, socket}
  end

  def join("screen:" <> room_id, _params, socket) do
    #send(self(), {:after_join, message})
    {:ok, %{game_id: socket.assigns.game_id}, socket}
  end

  def handle_info({:after_join, _message}, socket) do
    #broadcast! socket, "screen:started", %{game_id: game_id}
    #{:reply, {:ok, %{game_id: game_id}}, socket}
  end

  intercept ["player:joined"]

  def handle_out("player:joined", msg, socket) do
    IO.puts "got player:joined #{inspect msg}"
    push socket, "player:joined", msg
    {:noreply, socket}
  end

  def handle_in("msg", msg, socket) do
    IO.puts "got message from screen #{inspect msg}"
    game_id = socket.assigns.game_id
    Gj2020ex.Endpoint.broadcast! "players:#{game_id}", "player:msg", msg
    {:noreply, socket}
  end
end
