defmodule Game.PlayerChannel do
  use Phoenix.Channel

  alias Game.GameState

  def join("players:lobby", message, socket) do
    #players = GameState.players()
    #send(self(), {:after_join, message})
    {:ok, %{}, socket}
  end

  def join("players:" <> game_id, _params, socket) do
      socket = assign(socket, :game_id, game_id)
      Gj2020ex.Endpoint.broadcast! "screen:#{game_id}", "player:joined", %{
        player_id: socket.assigns.player_id,
      }
      {:ok, %{game_id: game_id}, socket}
  end

  def handle_in("msg", msg, socket) do
    IO.puts "got message #{inspect msg}"
    player_id = socket.assigns.player_id
    game_id = socket.assigns.game_id
    Gj2020ex.Endpoint.broadcast! "screen:#{game_id}", "player:message", %{
      player_id: socket.assigns.player_id,
      message: msg
    }
    {:noreply, socket}
  end

  intercept ["player:msg"]

  def handle_out("player:msg", msg, socket) do

    IO.puts "sending message to players #{inspect msg}"
    if Map.has_key?(msg, "player_id") do
      if socket.assigns.player_id == Map.get(msg, "player_id") do
        push(socket, "player:msg", msg)
      end
    else
      IO.puts "sending"
      push(socket, "player:msg", msg)
    end
    {:noreply, socket}
  end
end
