defmodule Game.GameState do

  def start_link do
    Agent.start_link(fn -> %{next_game_id: 1, games: %{}} end, name: __MODULE__)
  end

  def next_game_id do
    Agent.get_and_update(__MODULE__, fn state = %{next_game_id: id} ->
      {id, %{state|next_game_id: id + 1}}
    end)
  end

end
